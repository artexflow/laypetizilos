#include <stdlib.h>
#include "island.h"

static t_list	*fill_line(char *line, t_list *lst)
{
	t_list		*tmp;
	int			column;

	column = 0;
	while (lst != NULL && lst->file_char != '\n')
	{
		line[column++] = lst->file_char;
		tmp = lst;
		lst = lst->next;
		free(tmp);
	}
	if (lst->file_char == '\n')
	{
		tmp = lst;
		lst = lst->next;
		free(tmp);
	}
	return (lst);
}

char			**build_map(int line_count, int line_size, t_list *lst)
{
	char		**map;
	int			line;
	int			column;

	line = column = 0;
	if (!(map = NULL) && !(map = malloc(sizeof(char *) * line_count + 1)))
		return (NULL);
	map[line_count] = NULL;
	while (line < line_count)
	{
		if (!(map[line] = NULL)
			&& !(map[line] = malloc(sizeof(char) * line_size + 1)))
			return (NULL);
		else
		{
			map[line_size] = '\0';
			lst = fill_line(map[line], lst);
		}
		++line;
	}
	return (map);
}
