#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include "island.h"

static t_list	*add_char(t_list *tail, char c)
{
	t_list		*new_elem;

	if (!(new_elem = NULL) && !(new_elem = malloc(sizeof(t_list))))
		return (NULL);
	new_elem->file_char = c;
	new_elem->next = NULL;
	tail->next = new_elem;
	return (new_elem);
}

static t_list	*init_list(char c)
{
	t_list		*new_elem;

	if (!(new_elem = NULL) && !(new_elem = malloc(sizeof(t_list))))
		return (NULL);
	new_elem->file_char = c;
	new_elem->next = NULL;
	return (new_elem);
}

static t_list	*build_list(int fd)
{
	t_list		*lst_head;
	t_list		*tmp;
	char		buf[1];
	int			turn;

	lst_head = tmp = NULL;
	turn = 0;
	while (read(fd, buf, 1) > 0)
	{
		if (turn > 0 && !(tmp = add_char(tmp, *buf)))
			return (NULL);
		else if (turn == 0)
		{
			if (!(lst_head = init_list(*buf)))
				return (NULL);
			tmp = lst_head;
			++turn;
		}
	}
	return (lst_head);
}

int				main(int ac, const char **av)
{
	int			fd;
	int			line_count;
	int			line_size;
	char		**map;
	t_list		*shredded_file;

	line_count = 0;
	if (ac != 2
		|| (fd = open(av[1], O_RDONLY)) < 0
		|| !(shredded_file = build_list(fd))
		|| (line_size = is_valid(shredded_file, &line_count)) <= 0
		|| !(map = build_map(line_count, line_size, shredded_file)))
	{
		write(1, "\n", 1);
		return (0);
	}
	count_island(map);
	return (0);
}
