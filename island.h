#ifndef ISLAND_H
# define ISLAND_H

typedef struct		s_list
{
	char			file_char;
	struct s_list	*next;
}					t_list;

int					is_valid(t_list *lst, int *line_count);
char				**build_map(int line_count, int line_size, t_list *lst);
void				count_island(char **map);

#endif /* !ISLAND_H */
