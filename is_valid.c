#include <stdlib.h>
#include "island.h"

static int		control(int valid_size, int *size, char c)
{
	if (c != '.' && c != 'X' && c != '\n')
		return (0);
	if (c != '\n')
		++*size;
	else if (*size != valid_size)
		return (0);
	else
		*size = 0;
	return (1);
}

int				is_valid(t_list *lst, int *line_count)
{
	t_list		*tmp;
	int			line_size;
	int			valid_line_size;

	tmp = lst;
	valid_line_size = line_size = 0;
	while (tmp != NULL)
	{
		if (tmp->file_char != '\n' && ++valid_line_size)
			tmp = tmp->next;
		else
			break ;
	}
	tmp = lst;
	while (valid_line_size > 0 && tmp != NULL)
	{
		if (!control(valid_line_size, &line_size, tmp->file_char))
			return (0);
		else if (tmp->file_char == '\n')
			++*line_count;
		tmp = tmp->next;
	}
	return (valid_line_size);
}
