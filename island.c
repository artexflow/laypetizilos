#include <unistd.h>

static void		print_map(char **map)
{
	int			i;
	int			j;

	i = j = 0;
	while (map[i] != NULL)
	{
		j = 0;
		while (map[i][j] != '\0')
		{
			write(1, &map[i][j], 1);
			++j;
		}
		write(1, "\n", 1);
		++i;
	}
}

static void		write_island(char **map, char island_number, int i, int j)
{
	if (map[i][j] == 'X')
	{
		map[i][j] = island_number;
		if (map[i][j + 1] != '\0')
			write_island(map, island_number, i, j + 1);
		if (j - 1 >= 0)
			write_island(map, island_number, i, j - 1);
		if (map[i + 1] != NULL)
			write_island(map, island_number, i + 1, j);
		if (i - 1 >= 0)
			write_island(map, island_number, i - 1, j);
	}
}

void			count_island(char **map)
{
	int			i;
	int			j;
	char		island_number;

	i = j = 0;
	island_number = '0';
	while (map[i] != NULL)
	{
		j = 0;
		while (map[i][j] != '\0')
		{
			if (map[i][j] == 'X')
			{
				write_island(map, island_number, i, j);
				++island_number;
			}
			++j;
		}
		++i;
	}
	print_map(map);
}
